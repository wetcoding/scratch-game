package com.wetcoding.scratchgame.dto;

import com.wetcoding.scratchgame.configuration.dto.combinations.CombinationParameters;

public record AppliedCombination(String name, CombinationParameters combinationParameters) {

}
