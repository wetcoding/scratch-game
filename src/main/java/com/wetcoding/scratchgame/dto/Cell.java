package com.wetcoding.scratchgame.dto;

import com.wetcoding.scratchgame.configuration.dto.SymbolType;

public record Cell(String symbol, SymbolType type) {
    public static Cell ofStandard(String symbol) {
        return new Cell(symbol, SymbolType.STANDARD);
    }

    public static Cell ofBonus(String symbol) {
        return new Cell(symbol, SymbolType.BONUS);
    }
}
