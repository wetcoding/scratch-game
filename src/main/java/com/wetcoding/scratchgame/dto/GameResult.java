package com.wetcoding.scratchgame.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.util.List;
import java.util.Map;

@Builder
public record GameResult(
        String[][] matrix,
        @JsonProperty("applied_winning_combinations")
        Map<String, List<String>> appliedWinningCombinations,
        @JsonProperty("applied_bonus_symbol")
        String appliedBonusSymbol,
        Float reward) {
}
