package com.wetcoding.scratchgame.calculation;

import com.wetcoding.scratchgame.configuration.dto.Configuration;
import com.wetcoding.scratchgame.configuration.dto.SymbolDefinition;
import com.wetcoding.scratchgame.configuration.dto.SymbolType;
import com.wetcoding.scratchgame.calculation.checker.CombinationCheckerFactory;
import com.wetcoding.scratchgame.dto.AppliedCombination;
import com.wetcoding.scratchgame.dto.Cell;
import com.wetcoding.scratchgame.dto.GameResult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class ResultCalculator {


    public GameResult calculate(Float betAmount, Configuration configuration, Cell[][] gameBoard) {
        Map<String, Integer> boardSymbols = calculateBoardSymbols(gameBoard);

        Map<String, List<AppliedCombination>> combinationPerSymbol = new HashMap<>();


        boardSymbols.forEach((symbol, count) -> {
            List<AppliedCombination> symbolCombinations = new ArrayList<>();

            configuration.winCombinations()
                    .forEach((name, params) -> symbolCombinations
                            .addAll(CombinationCheckerFactory.getChecker(params)
                                            .checkCombination(symbol, count, gameBoard, name, params)));

            if (!symbolCombinations.isEmpty()) {
                combinationPerSymbol.put(symbol, symbolCombinations);
            }
        });

        Optional<Cell> bonusCell = getBonusCell(gameBoard);

        SymbolDefinition bonusSymbol = bonusCell
                .map(cell-> configuration.symbols().get(cell.symbol()))
                .orElse(null);

        float reward = calculateResultAmount(betAmount, combinationPerSymbol, bonusSymbol, configuration)
                .floatValue();


        String appliedBonus = bonusCell.map(Cell::symbol)
                .filter(cell -> !combinationPerSymbol.isEmpty())
                .orElse(null);

        return GameResult.builder()
                .appliedWinningCombinations(transformAppliedCombination(combinationPerSymbol))
                .reward(reward)
                .appliedBonusSymbol(appliedBonus)
                .matrix(getMatrix(gameBoard))
                .build();
    }

    private Map<String, List<String>> transformAppliedCombination(Map<String, List<AppliedCombination>> combinationPerSymbol) {
        return combinationPerSymbol.keySet()
                .stream()
                .collect(Collectors.toMap(name -> name, name -> combinationPerSymbol.get(name).stream().map(AppliedCombination::name).toList()));
    }

    private BigDecimal calculateResultAmount(Float betAmount, Map<String, List<AppliedCombination>> combinationPerSymbol,
                                             SymbolDefinition bonus, Configuration configuration) {
        if (combinationPerSymbol.isEmpty()) {
            return BigDecimal.ZERO;
        }


        BigDecimal result = combinationPerSymbol.entrySet().stream()
                .map(entry -> {
                    SymbolDefinition symbolDefinition = Objects.requireNonNull(configuration.symbols().get(entry.getKey()));
                    BigDecimal symbolMultiplier = BigDecimal.valueOf(symbolDefinition.rewardMultiplier());

                    return calculateForSymbol(BigDecimal.valueOf(betAmount), symbolMultiplier, entry.getValue());
                })
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);

        if (bonus == null) {
            return result;
        }


        return switch (bonus.impact()) {
            case EXTRA_BONUS -> result.add(BigDecimal.valueOf(bonus.extra()));
            case MULTIPLY_REWARD -> result.multiply(BigDecimal.valueOf(bonus.rewardMultiplier()));
            case MISS -> result;
        };
    }

    private BigDecimal calculateForSymbol(BigDecimal betAmount, BigDecimal symbolMultiplier, List<AppliedCombination> appliedCombinations) {
        BigDecimal combinationMultiplier = appliedCombinations.stream()
                .map(comb -> BigDecimal.valueOf(comb.combinationParameters().rewardMultiplier()))
                .reduce(BigDecimal::multiply)
                .orElse(BigDecimal.ONE);

        return betAmount
                .multiply(symbolMultiplier)
                .multiply(combinationMultiplier);
    }

    private String[][] getMatrix(Cell[][] gameBoard) {
        String[][] result = new String[gameBoard.length][gameBoard[0].length];
        for (int row = 0; row < gameBoard.length; row++) {
            for (int col = 0; col < gameBoard[0].length; col++) {
                result[row][col] = gameBoard[row][col].symbol();
            }
        }

        return result;
    }

    private Optional<Cell> getBonusCell(Cell[][] gameBoard) {
        for (int row = 0; row < gameBoard.length; row++) {
            for (int col = 0; col < gameBoard[0].length; col++) {
                Cell boardCell = gameBoard[row][col];
                if (boardCell.type() == SymbolType.BONUS) {
                    return Optional.of(boardCell);
                }
            }
        }

        return Optional.empty();
    }

    private Map<String, Integer> calculateBoardSymbols(Cell[][] gameBoard) {
        Map<String, Integer> result = new HashMap<>();
        for (int row = 0; row < gameBoard.length; row++) {
            for (int col = 0; col < gameBoard[0].length; col++) {
                Cell boardCell = gameBoard[row][col];
                if (boardCell.type() == SymbolType.STANDARD) {
                    if (result.containsKey(boardCell.symbol())) {
                        result.computeIfPresent(boardCell.symbol(), (k, v) -> ++v);
                    } else {
                        result.put(boardCell.symbol(), 1);
                    }
                }
            }
        }

        return result;
    }


}
