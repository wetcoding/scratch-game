package com.wetcoding.scratchgame.calculation.checker;

import com.wetcoding.scratchgame.configuration.dto.combinations.CombinationParameters;
import com.wetcoding.scratchgame.dto.AppliedCombination;
import com.wetcoding.scratchgame.dto.Cell;

import java.util.ArrayList;
import java.util.List;

public class RtlDiagonallySameCombinationChecker extends CombinationChecker {
    @Override
    public List<AppliedCombination> checkCombination(String symbol, Integer appearanceCount, Cell[][] gameBoard,
                                                     String name, CombinationParameters combinationParameters) {
        List<AppliedCombination> result = new ArrayList<>();


        boolean allSame = true;
        for (int row = 0; row < gameBoard.length; row++) {
            int col = gameBoard.length - 1 - row;
            if (!gameBoard[row][col].symbol().equals(symbol) || !isCellCovered(combinationParameters, row, col)
            ) {
                allSame = false;
                break;
            }
        }
        if (allSame) {
            result.add(new AppliedCombination(name, combinationParameters));
        }


        return result;
    }
}
