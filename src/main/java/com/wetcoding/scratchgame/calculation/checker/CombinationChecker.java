package com.wetcoding.scratchgame.calculation.checker;

import com.wetcoding.scratchgame.configuration.dto.combinations.CombinationParameters;
import com.wetcoding.scratchgame.dto.AppliedCombination;
import com.wetcoding.scratchgame.dto.Cell;

import java.util.List;

public abstract class CombinationChecker {
    public abstract List<AppliedCombination> checkCombination(String symbol, Integer appearanceCount, Cell[][] gameBoard,
                                              String name, CombinationParameters combinationParameters);

    protected boolean isCellCovered(CombinationParameters combinationParameters, int row, int cell) {
        return combinationParameters.coveredAreas()
                .stream()
                .anyMatch(area -> area.row() == row && area.cell() == cell);
    }
}
