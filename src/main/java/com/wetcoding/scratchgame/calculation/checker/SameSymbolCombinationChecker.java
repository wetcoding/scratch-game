package com.wetcoding.scratchgame.calculation.checker;


import com.wetcoding.scratchgame.configuration.dto.combinations.CombinationParameters;
import com.wetcoding.scratchgame.dto.AppliedCombination;
import com.wetcoding.scratchgame.dto.Cell;

import java.util.ArrayList;
import java.util.List;

class SameSymbolCombinationChecker extends CombinationChecker {


    @Override
    public List<AppliedCombination> checkCombination(String symbol, Integer appearanceCount, Cell[][] gameBoard,
                                                     String name, CombinationParameters combinationParameters) {
        List<AppliedCombination> result = new ArrayList<>();

        if (combinationParameters.count().equals(appearanceCount)) {
            result.add(new AppliedCombination(name, combinationParameters));
        }

        return result;
    }
}
