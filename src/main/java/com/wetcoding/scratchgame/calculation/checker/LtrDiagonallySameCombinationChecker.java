package com.wetcoding.scratchgame.calculation.checker;

import com.wetcoding.scratchgame.configuration.dto.combinations.CombinationParameters;
import com.wetcoding.scratchgame.dto.AppliedCombination;
import com.wetcoding.scratchgame.dto.Cell;

import java.util.ArrayList;
import java.util.List;

public class LtrDiagonallySameCombinationChecker extends CombinationChecker {
    @Override
    public List<AppliedCombination> checkCombination(String symbol, Integer appearanceCount, Cell[][] gameBoard,
                                                     String name, CombinationParameters combinationParameters) {
        List<AppliedCombination> result = new ArrayList<>();

        boolean allSame = true;
        for (int i = 0; i < gameBoard.length; i++) {
            if (!gameBoard[i][i].symbol().equals(symbol) || !isCellCovered(combinationParameters, i, i)) {
                allSame = false;
                break;
            }
        }
        if (allSame) {
            result.add(new AppliedCombination(name, combinationParameters));
        }


        return result;
    }
}
