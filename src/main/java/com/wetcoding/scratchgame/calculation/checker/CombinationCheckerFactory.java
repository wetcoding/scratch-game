package com.wetcoding.scratchgame.calculation.checker;

import com.wetcoding.scratchgame.configuration.dto.combinations.CombinationParameters;

public class CombinationCheckerFactory {
    private final static SameSymbolCombinationChecker sameSymbolChecker = new SameSymbolCombinationChecker();
    private final static VerticallySameCombinationChecker verticallySameChecker = new VerticallySameCombinationChecker();
    private final static HorizontallySameCombinationChecker horizontallySameChecker = new HorizontallySameCombinationChecker();
    private final static LtrDiagonallySameCombinationChecker ltrDiagonallySameChecker = new LtrDiagonallySameCombinationChecker();
    private final static RtlDiagonallySameCombinationChecker rtlDiagonallySameChecker = new RtlDiagonallySameCombinationChecker();

    public static CombinationChecker getChecker(CombinationParameters combinationParameters) {
        return switch (combinationParameters.group()) {
            case SAME_SYMBOLS -> sameSymbolChecker;
            case VERTICALLY_LINEAR_SYMBOLS -> verticallySameChecker;
            case HORIZONTALLY_LINEAR_SYMBOLS -> horizontallySameChecker;
            case LTR_DIAGONALLY_LINEAR_SYMBOLS -> ltrDiagonallySameChecker;
            case RTL_DIAGONALLY_LINEAR_SYMBOLS -> rtlDiagonallySameChecker;
        };
    }
}
