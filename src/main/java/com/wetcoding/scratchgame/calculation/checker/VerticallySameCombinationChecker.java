package com.wetcoding.scratchgame.calculation.checker;

import com.wetcoding.scratchgame.configuration.dto.combinations.CombinationParameters;
import com.wetcoding.scratchgame.dto.AppliedCombination;
import com.wetcoding.scratchgame.dto.Cell;

import java.util.ArrayList;
import java.util.List;

class VerticallySameCombinationChecker extends CombinationChecker {
    @Override
    public List<AppliedCombination> checkCombination(String symbol, Integer appearanceCount, Cell[][] gameBoard,
                                                     String name, CombinationParameters combinationParameters) {
        List<AppliedCombination> result = new ArrayList<>();


        for (int col = 0; col < gameBoard[0].length; col++) {
            boolean sameInColumn = true;
            for (int row = 0; row < gameBoard.length; row++) {
                if (!gameBoard[row][col].symbol().equals(symbol) || !isCellCovered(combinationParameters, row, col)) {
                    sameInColumn = false;
                    break;
                }
            }

            if (sameInColumn) {
                result.add(new AppliedCombination(name, combinationParameters));
            }
        }

        return result;
    }
}
