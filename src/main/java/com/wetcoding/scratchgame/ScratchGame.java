package com.wetcoding.scratchgame;

import com.wetcoding.scratchgame.calculation.ResultCalculator;
import com.wetcoding.scratchgame.configuration.ConfigurationReader;
import com.wetcoding.scratchgame.configuration.dto.Configuration;
import com.wetcoding.scratchgame.configuration.validator.SimpleConfigurationValidator;
import com.wetcoding.scratchgame.dto.Cell;
import com.wetcoding.scratchgame.dto.GameResult;
import com.wetcoding.scratchgame.generator.GameBoardGenerator;
import com.wetcoding.scratchgame.generator.random.SimpleRandomGenerator;

public class ScratchGame {

    public GameResult play(Float bettingAmount, String configurationPath) {
        Configuration configuration = new ConfigurationReader(new SimpleConfigurationValidator())
                .read(configurationPath);

        Cell[][] gameBoard = new GameBoardGenerator(new SimpleRandomGenerator())
                .generate(configuration);

        return new ResultCalculator().calculate(bettingAmount, configuration, gameBoard);
    }


}
