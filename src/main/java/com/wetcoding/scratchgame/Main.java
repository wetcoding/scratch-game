package com.wetcoding.scratchgame;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wetcoding.scratchgame.dto.GameResult;

public class Main {
    private static final String CONFIG_TOKEN = "--config";
    private static final String BETTING_AMOUNT_TOKEN = "--betting-amount";

    public static void main(String[] args) throws Exception {


        String configPath = null;
        Float bettingAmount = null;

        for (int i = 0; i < args.length; i++) {
            if (configPath == null && args[i].equals(CONFIG_TOKEN) && i < args.length - 1) {
                configPath = args[i + 1];
            } else if (bettingAmount == null && args[i].equals(BETTING_AMOUNT_TOKEN) && i < args.length - 1) {
                bettingAmount = Float.valueOf(args[i + 1]);
            }
        }

        if (configPath == null) {
            System.out.println("Error: Configuration path not provided!");
            return;
        }
        if (bettingAmount == null) {
            System.out.println("Error: Betting amount not provided!");
            return;
        }

        ScratchGame game = new ScratchGame();
        GameResult gameResult = game.play(bettingAmount, configPath);
        System.out.println(new ObjectMapper().writeValueAsString(gameResult));
    }

}
