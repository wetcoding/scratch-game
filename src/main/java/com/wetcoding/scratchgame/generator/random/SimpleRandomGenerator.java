package com.wetcoding.scratchgame.generator.random;

import java.util.Random;

public class SimpleRandomGenerator implements RandomGenerator{
    private final Random random = new Random();

    @Override
    public int nextInt(int max) {
        return random.nextInt(max);
    }
}
