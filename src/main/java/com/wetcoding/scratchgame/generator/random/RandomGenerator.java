package com.wetcoding.scratchgame.generator.random;

public interface RandomGenerator {
    int nextInt(int max);
}
