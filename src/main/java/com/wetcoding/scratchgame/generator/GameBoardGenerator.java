package com.wetcoding.scratchgame.generator;

import com.wetcoding.scratchgame.configuration.dto.Configuration;
import com.wetcoding.scratchgame.configuration.dto.ImpactType;
import com.wetcoding.scratchgame.configuration.dto.SymbolDefinition;
import com.wetcoding.scratchgame.configuration.dto.SymbolType;
import com.wetcoding.scratchgame.configuration.dto.probabilities.BonusProbabilities;
import com.wetcoding.scratchgame.configuration.dto.probabilities.CellSymbolProbabilities;
import com.wetcoding.scratchgame.dto.Cell;
import com.wetcoding.scratchgame.generator.random.RandomGenerator;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

public class GameBoardGenerator {
    private final RandomGenerator random;

    public GameBoardGenerator(RandomGenerator random) {
        this.random = random;
    }

    public Cell[][] generate(Configuration configuration) {
        int rowCount = configuration.rows() != null
                ? configuration.rows()
                : getRowCountFromProbabilities(configuration);

        int columnCount = configuration.columns() != null
                ? configuration.columns()
                : getColumnCountFromProbabilities(configuration);

        Cell[][] gameBoard = new Cell[rowCount][columnCount];

        Optional<GeneratedBonus> generatedBonus = generateBonus(configuration, rowCount, columnCount);

        for (int row = 0; row < rowCount; row++) {
            for (int col = 0; col < columnCount; col++) {
                Cell cell = generatedBonus.isPresent() && isBonusCell(row, col, generatedBonus.get())
                        ? generatedBonus.get().bonus()
                        : calculateCell(configuration, row, col);

                gameBoard[row][col] = cell;
            }
        }

        return gameBoard;
    }

    private boolean isBonusCell(int row, int col, GeneratedBonus generatedBonus) {
        return generatedBonus.row() == row && generatedBonus.coll() == col;
    }

    private Cell calculateCell(Configuration configuration, int col, int row) {
        CellSymbolProbabilities probabilities = getCellProbabilities(configuration, col, row);
        return getNextCellByProbabilities(probabilities.symbols(), SymbolType.STANDARD);
    }

    private Optional<GeneratedBonus> generateBonus(Configuration configuration, int rowCount, int columnCount) {
        return Optional.ofNullable(configuration.probabilities().bonusSymbols())
                .map(BonusProbabilities::symbols)
                .filter(map -> !map.isEmpty())
                .map(map -> getNextCellByProbabilities(map, SymbolType.BONUS))
                .filter(bonusCell -> {
                    SymbolDefinition bonusSymbol = Objects.requireNonNull(configuration.symbols().get(bonusCell.symbol()));
                    return bonusSymbol.impact() != ImpactType.MISS;
                })
                .map(bonusCell -> {
                    int row = random.nextInt(rowCount);
                    int col = random.nextInt(columnCount);
                    return new GeneratedBonus(row, col, bonusCell);
                });
    }

    private Cell getNextCellByProbabilities(Map<String, Integer> probabilitiesMap, SymbolType symbolType) {
        Integer total = probabilitiesMap.values().stream().reduce(Integer::sum).orElseThrow();

        int randomVal = random.nextInt(total);

        Iterator<Map.Entry<String, Integer>> iterator = probabilitiesMap.entrySet()
                .stream().sorted(Map.Entry.comparingByValue())
                .iterator();

        int prevProbability = 0;

        while (iterator.hasNext()) {
            Map.Entry<String, Integer> next = iterator.next();

            Integer probability = next.getValue();


            if (randomVal < prevProbability + probability) {
                return new Cell(next.getKey(), symbolType);
            }

            prevProbability = prevProbability + probability;
        }

        throw new RuntimeException("Unable to resolve Cell by probabilities");
    }

    private CellSymbolProbabilities getCellProbabilities(Configuration configuration, int row, int col) {
        return configuration.probabilities().standardSymbols()
                .stream().filter(probabilities -> probabilities.column().equals(col) && probabilities.row().equals(row))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("Probabilities not found for [row = %d, col = %s]", row, col)));
    }


    private int getRowCountFromProbabilities(Configuration configuration) {
        return getMaxAttribute(CellSymbolProbabilities::row, configuration);
    }

    private int getColumnCountFromProbabilities(Configuration configuration) {
        return getMaxAttribute(CellSymbolProbabilities::column, configuration);
    }

    private int getMaxAttribute(Function<CellSymbolProbabilities, Integer> attributeGetter, Configuration configuration) {
        return configuration.probabilities().standardSymbols()
                .stream().map(attributeGetter)
                .max(Integer::compareTo)
                .map(i -> i + 1)
                .orElseThrow();
    }

    record GeneratedBonus(int row, int coll, Cell bonus) {

    }

}
