package com.wetcoding.scratchgame.configuration.validator;

import com.wetcoding.scratchgame.configuration.dto.Configuration;

public interface ConfigurationValidator {
    void validate(Configuration configuration);
}
