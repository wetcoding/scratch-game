package com.wetcoding.scratchgame.configuration.validator;

import com.wetcoding.scratchgame.configuration.dto.Configuration;

public class SimpleConfigurationValidator implements ConfigurationValidator {
    @Override
    public void validate(Configuration configuration) {
        // check column/row number defined explicitly or from probabilities
        // check symbols/probabilities defined
        // check row == column (to calculate diagonally)
    }
}
