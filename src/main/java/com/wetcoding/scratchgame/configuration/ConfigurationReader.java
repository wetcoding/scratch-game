package com.wetcoding.scratchgame.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wetcoding.scratchgame.configuration.dto.Configuration;
import com.wetcoding.scratchgame.configuration.validator.ConfigurationValidator;

import java.io.File;
import java.io.IOException;

public class ConfigurationReader {
    private final ConfigurationValidator validator;

    public ConfigurationReader(ConfigurationValidator validator) {
        this.validator = validator;
    }

    public Configuration read(String path) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Configuration configuration = objectMapper.readValue(new File(path), Configuration.class);
            validator.validate(configuration);

            return configuration;
        } catch (IOException e) {
            throw new RuntimeException("Error reading configuration file", e);
        }
    }
}
