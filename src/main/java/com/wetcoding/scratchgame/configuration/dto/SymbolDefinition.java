package com.wetcoding.scratchgame.configuration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public record SymbolDefinition(
        @JsonProperty("reward_multiplier")
        Float rewardMultiplier,
        Float extra,
        SymbolType type,
        ImpactType impact
) {

}
