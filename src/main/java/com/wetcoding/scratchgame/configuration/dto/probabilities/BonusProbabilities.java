package com.wetcoding.scratchgame.configuration.dto.probabilities;

import lombok.Builder;

import java.util.Map;

@Builder
public record BonusProbabilities(
        Map<String, Integer> symbols
) {
}
