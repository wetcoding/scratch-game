package com.wetcoding.scratchgame.configuration.dto.probabilities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.util.List;

@Builder
public record Probabilities(
        @JsonProperty("standard_symbols")
        List<CellSymbolProbabilities> standardSymbols,
        @JsonProperty("bonus_symbols")
        BonusProbabilities bonusSymbols
) {
}
