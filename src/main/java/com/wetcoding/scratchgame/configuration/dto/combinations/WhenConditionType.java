package com.wetcoding.scratchgame.configuration.dto.combinations;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum WhenConditionType {
    @JsonProperty("same_symbols")
    SAME_SYMBOLS,
    @JsonProperty("linear_symbols")
    LINEAR_SYMBOLS
}
