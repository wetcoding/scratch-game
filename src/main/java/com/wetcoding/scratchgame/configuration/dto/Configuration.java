package com.wetcoding.scratchgame.configuration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wetcoding.scratchgame.configuration.dto.combinations.CombinationParameters;
import com.wetcoding.scratchgame.configuration.dto.probabilities.Probabilities;
import lombok.Builder;

import java.util.Map;

@Builder
public record Configuration(
        Integer columns,
        Integer rows,
        Map<String, SymbolDefinition> symbols,
        Probabilities probabilities,
        @JsonProperty("win_combinations")
        Map<String, CombinationParameters> winCombinations
) {


}
