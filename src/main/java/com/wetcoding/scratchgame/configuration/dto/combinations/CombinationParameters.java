package com.wetcoding.scratchgame.configuration.dto.combinations;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Builder
public record CombinationParameters(
        @JsonProperty("reward_multiplier")
        Float rewardMultiplier,
        WhenConditionType when,
        Integer count,
        CombinationGroupType group,
        @JsonProperty("covered_areas")
        @JsonDeserialize(using = CoveredAreasDeserializer.class)
        Set<CellPosition> coveredAreas
) {
    public record CellPosition(int row, int cell) {

    }

    public static class CoveredAreasDeserializer extends JsonDeserializer<Set<CellPosition>> {
        @Override
        public Set<CellPosition> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            Set<CellPosition> coveredAreas = new HashSet<>();
            JsonNode treeNode = jsonParser.getCodec().readTree(jsonParser);
            for (int i = 0; i < treeNode.size(); i++) {
                JsonNode array = treeNode.get(i);
                for (int k = 0; k < array.size(); k++) {
                    String val = array.get(k).asText();
                    String[] split = val.split(":");
                    coveredAreas.add(new CellPosition(Integer.parseInt(split[0]), Integer.parseInt(split[1])));
                }

            }
            return coveredAreas;
        }
    }
}
