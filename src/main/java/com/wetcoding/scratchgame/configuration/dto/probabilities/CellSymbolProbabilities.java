package com.wetcoding.scratchgame.configuration.dto.probabilities;

import lombok.Builder;

import java.util.Map;

@Builder
public record CellSymbolProbabilities(
        Integer column,
        Integer row,
        Map<String, Integer> symbols
) {
}
