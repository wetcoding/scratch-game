package com.wetcoding.scratchgame;

import com.wetcoding.scratchgame.dto.GameResult;
import org.junit.jupiter.api.Test;

import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;

class ScratchGameTest {

    @Test
    void play() {
        ScratchGame scratchGame = new ScratchGame();

        URL resource = ScratchGameTest.class.getClassLoader().getResource("config.json");
        GameResult gameResult = scratchGame.play(100f, resource.getPath().toString());

        assertThat(gameResult).isNotNull();
        assertThat(gameResult.matrix()).isNotEmpty();
    }
}