package com.wetcoding.scratchgame.game;

import com.wetcoding.scratchgame.calculation.ResultCalculator;
import com.wetcoding.scratchgame.dto.Cell;
import com.wetcoding.scratchgame.dto.GameResult;
import com.wetcoding.scratchgame.utils.TestConfigurationBuilder;
import lombok.Builder;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class ResultCalculatorTest {
    private final ResultCalculator resultCalculator = new ResultCalculator();

    static Stream<CalculationTestCase> testCases() {
        return Stream.of(
                CalculationTestCase.builder()
                        .gameBoard(new Cell[][]{
                                {Cell.ofStandard("A"), Cell.ofStandard("B"), Cell.ofStandard("C")},
                                {Cell.ofStandard("D"), Cell.ofStandard("E"), Cell.ofStandard("F")},
                                {Cell.ofBonus("+1000"), Cell.ofStandard("B"), Cell.ofStandard("C")}})
                        .expectedCombinations(Collections.emptyMap())
                        .expectedReward(0f)
                        .build(),
                CalculationTestCase.builder()
                        .gameBoard(new Cell[][]{
                                {Cell.ofStandard("A"), Cell.ofStandard("A"), Cell.ofStandard("C")},
                                {Cell.ofStandard("E"), Cell.ofStandard("B"), Cell.ofBonus("5x")},
                                {Cell.ofStandard("F"), Cell.ofStandard("D"), Cell.ofStandard("A")}})
                        .expectedCombinations(Map.of("A", List.of("same_symbol_3_times")))
                        //100 * (A) 50 * (same_symbol_3_times) 1 * 5 = 25000
                        .expectedReward(25000f)
                        .expectedAppliedBonus("5x")
                        .build(),
                CalculationTestCase.builder()
                        .gameBoard(new Cell[][]{
                                {Cell.ofStandard("A"), Cell.ofStandard("A"), Cell.ofStandard("B")},
                                {Cell.ofStandard("B"), Cell.ofStandard("E"), Cell.ofStandard("A")},
                                {Cell.ofStandard("A"), Cell.ofStandard("B"), Cell.ofBonus("+1000")}})
                        //100 * (A) 50 * (same_symbol_4_times) 1.5 + 100 * (B) 25 * (same_symbol_3_times) 1 + 1000 = 11000
                        .expectedCombinations(Map.of("A", List.of("same_symbol_4_times"),
                                                     "B", List.of("same_symbol_3_times")))
                        .expectedReward(11000f)
                        .expectedAppliedBonus("+1000")
                        .build(),
                CalculationTestCase.builder()
                        .gameBoard(new Cell[][]{
                                {Cell.ofStandard("A"), Cell.ofStandard("B"), Cell.ofStandard("C")},
                                {Cell.ofStandard("B"), Cell.ofStandard("B"), Cell.ofBonus("10x")},
                                {Cell.ofStandard("B"), Cell.ofStandard("D"), Cell.ofStandard("B")}})
                        .expectedCombinations(Map.of("B", List.of("same_symbol_5_times")))
                        //100 * (B) 25 * (same_symbol_5_times) 2 * 10 = 50000
                        .expectedReward(50000f)
                        .expectedAppliedBonus("10x")
                        .build(),
                CalculationTestCase.builder()
                        .gameBoard(new Cell[][]{
                                {Cell.ofStandard("A"), Cell.ofStandard("A"), Cell.ofStandard("C")},
                                {Cell.ofStandard("C"), Cell.ofStandard("C"), Cell.ofStandard("A")},
                                {Cell.ofStandard("A"), Cell.ofStandard("A"), Cell.ofStandard("A")}})
                        .expectedCombinations(Map.of("A", List.of("same_symbol_6_times", "same_symbols_horizontally"),
                                                     "C", List.of("same_symbol_3_times")))
                        //100 * (A) 50 * (same_symbol_6_times) 3 * (same_symbols_horizontally) 2 + 100 * (C) 10 * (same_symbol_3_times) 1 = 31000
                        .expectedReward(31000f)
                        .build(),
                CalculationTestCase.builder()
                        .gameBoard(new Cell[][]{
                                {Cell.ofStandard("D"), Cell.ofStandard("D"), Cell.ofStandard("D")},
                                {Cell.ofBonus("+500"), Cell.ofStandard("F"), Cell.ofStandard("D")},
                                {Cell.ofStandard("D"), Cell.ofStandard("D"), Cell.ofStandard("D")}})
                        .expectedCombinations(Map.of("D", List.of("same_symbol_7_times", "same_symbols_horizontally", "same_symbols_horizontally", "same_symbols_vertically")))
                        //100 * (D) 5 * (same_symbol_7_times) 5 * (same_symbols_horizontally * 2) 4 * (same_symbols_vertically) 2 + 500 = 10500
                        .expectedReward(20500f)
                        .expectedAppliedBonus("+500")
                        .build(),
                CalculationTestCase.builder()
                        .gameBoard(new Cell[][]{
                                {Cell.ofStandard("E"), Cell.ofStandard("E"), Cell.ofStandard("E")},
                                {Cell.ofStandard("E"), Cell.ofBonus("10x"), Cell.ofStandard("E")},
                                {Cell.ofStandard("E"), Cell.ofStandard("E"), Cell.ofStandard("E")}})
                        .expectedCombinations(Map.of("E", List.of("same_symbol_8_times", "same_symbols_horizontally", "same_symbols_horizontally", "same_symbols_vertically", "same_symbols_vertically")))
                        //100 * (E) 3 * (same_symbol_8_times) 10 * (same_symbols_horizontally * 2) 4 * (same_symbols_vertically * 2) 4 * 10 = 480000
                        .expectedReward(480000f)
                        .expectedAppliedBonus("10x")
                        .build(),
                CalculationTestCase.builder()
                        .gameBoard(new Cell[][]{
                                {Cell.ofStandard("F"), Cell.ofStandard("B"), Cell.ofStandard("C")},
                                {Cell.ofStandard("D"), Cell.ofStandard("F"), Cell.ofStandard("F")},
                                {Cell.ofStandard("D"), Cell.ofStandard("B"), Cell.ofStandard("F")}})
                        .expectedCombinations(Map.of("F", List.of("same_symbol_4_times", "same_symbols_diagonally_left_to_right")))
                        //100 * (F) 1.5 * (same_symbol_4_times) 1.5 * (same_symbols_diagonally_left_to_right) 5 = 1125
                        .expectedReward(1125f)
                        .build(),
                CalculationTestCase.builder()
                        .gameBoard(new Cell[][]{
                                {Cell.ofStandard("B"), Cell.ofStandard("B"), Cell.ofStandard("C")},
                                {Cell.ofStandard("B"), Cell.ofStandard("C"), Cell.ofStandard("B")},
                                {Cell.ofStandard("C"), Cell.ofStandard("B"), Cell.ofStandard("B")}})
                        .expectedCombinations(Map.of(
                                "B", List.of("same_symbol_6_times"),
                                "C", List.of("same_symbol_3_times", "same_symbols_diagonally_right_to_left")))
                        //100 * (B) 25 * (same_symbol_6_times) 3 + 100 * (C) 10 * (same_symbol_3_times) 1 * (same_symbols_diagonally_right_to_left) 5 = 1125
                        .expectedReward(12500f)
                        .build(),
                CalculationTestCase.builder()
                        .gameBoard(new Cell[][]{
                                {Cell.ofStandard("A"), Cell.ofStandard("A"), Cell.ofStandard("A")},
                                {Cell.ofStandard("A"), Cell.ofStandard("A"), Cell.ofStandard("A")},
                                {Cell.ofStandard("A"), Cell.ofStandard("A"), Cell.ofStandard("A")}})
                        .expectedCombinations(Map.of("A", List.of("same_symbol_9_times",
                                                                  "same_symbols_horizontally", "same_symbols_horizontally", "same_symbols_horizontally",
                                                                  "same_symbols_vertically", "same_symbols_vertically", "same_symbols_vertically",
                                                                  "same_symbols_diagonally_left_to_right",
                                                                  "same_symbols_diagonally_right_to_left")))
                        //100 * (A) 50 * (same_symbol_9_times) 20 * (same_symbols_horizontally * 3) 8 * (same_symbols_vertically * 3) 8
                        // * (same_symbols_diagonally_left_to_right) 5 * (same_symbols_diagonally_right_to_left) 5 =
                        .expectedReward(160_000_000f)
                        .build()
        );
    }


    @ParameterizedTest
    @MethodSource("testCases")
    void shouldCalculateReward(CalculationTestCase testCase) {
        // when
        GameResult result = resultCalculator.calculate(100f,
                                                       TestConfigurationBuilder.getExpectedConfiguration(),
                                                       testCase.gameBoard());

        // then
        assertThat(result.appliedWinningCombinations()).hasSameSizeAs(testCase.expectedCombinations());
        result.appliedWinningCombinations().forEach((k, v) -> assertThat(v)
                .containsExactlyInAnyOrderElementsOf(testCase.expectedCombinations().get(k)));
        assertThat(result.reward()).isEqualTo(testCase.expectedReward());
        assertThat(result.appliedBonusSymbol()).isEqualTo(testCase.expectedAppliedBonus());
        validateMatrix(result.matrix(), testCase.gameBoard());
    }

    private void validateMatrix(String[][] actual, Cell[][] gameBoard) {
        SoftAssertions softly = new SoftAssertions();

        for (int row = 0; row < gameBoard.length; row++) {
            for (int col = 0; col < gameBoard[0].length; col++) {
                softly.assertThat(actual[row][col]).isEqualTo(gameBoard[row][col].symbol());
            }
        }
    }

    @Builder
    record CalculationTestCase(
            Cell[][] gameBoard,
            Map<String, List<String>> expectedCombinations,
            Float expectedReward,
            String expectedAppliedBonus) {

    }
}