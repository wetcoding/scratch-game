package com.wetcoding.scratchgame.configuration;

import com.wetcoding.scratchgame.configuration.dto.Configuration;
import com.wetcoding.scratchgame.utils.TestConfigurationBuilder;
import org.junit.jupiter.api.Test;

import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;

class ConfigurationReaderTest {


    @Test
    void shouldReadConfigurationFile() {
        URL resource = ConfigurationReaderTest.class.getClassLoader().getResource("config.json");
        ConfigurationReader reader = new ConfigurationReader(configuration -> {
            //no validation
        });

        Configuration configuration = reader.read(resource.getPath());

        assertThat(configuration).isEqualTo(TestConfigurationBuilder.getExpectedConfiguration());
    }


}