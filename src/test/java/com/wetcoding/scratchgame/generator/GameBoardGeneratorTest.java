package com.wetcoding.scratchgame.generator;

import com.wetcoding.scratchgame.configuration.dto.Configuration;
import com.wetcoding.scratchgame.configuration.dto.ImpactType;
import com.wetcoding.scratchgame.configuration.dto.SymbolDefinition;
import com.wetcoding.scratchgame.configuration.dto.probabilities.BonusProbabilities;
import com.wetcoding.scratchgame.configuration.dto.probabilities.CellSymbolProbabilities;
import com.wetcoding.scratchgame.configuration.dto.probabilities.Probabilities;
import com.wetcoding.scratchgame.dto.Cell;
import com.wetcoding.scratchgame.generator.random.RandomGenerator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class GameBoardGeneratorTest {

    @Test
    void testSymbolsSpread() {
        Configuration configuration = Configuration.builder()
                .probabilities(
                        Probabilities.builder()
                                .standardSymbols(getSymbolProbabilities(7, 3))
                                .bonusSymbols(BonusProbabilities.builder()
                                                      .symbols(Collections.emptyMap())
                                                      .build())
                                .build())
                .build();
        GameBoardGenerator generator = new GameBoardGenerator(new RandomGeneratorMock());
        Cell[][] board = generator.generate(configuration);

        Assertions.assertThat(getBoardSymbols(board))
                .isEqualTo(Map.of(
                        "A", 1,
                        "B", 2,
                        "C", 3,
                        "D", 4,
                        "E", 5,
                        "F", 6
                ));
    }

    @Test
    void testBonusSpread() {
        Configuration configuration = Configuration.builder()
                .symbols(Map.of(
                        "10x", SymbolDefinition.builder().impact(ImpactType.MULTIPLY_REWARD).build(),
                        "5x", SymbolDefinition.builder().impact(ImpactType.MULTIPLY_REWARD).build(),
                        "+1000", SymbolDefinition.builder().impact(ImpactType.EXTRA_BONUS).build(),
                        "+500", SymbolDefinition.builder().impact(ImpactType.EXTRA_BONUS).build(),
                        "MISS", SymbolDefinition.builder().impact(ImpactType.MISS).build()))
                .probabilities(
                        Probabilities.builder()
                                .standardSymbols(getSymbolProbabilities(3, 3))
                                .bonusSymbols(BonusProbabilities.builder()
                                                      .symbols(Map.of(
                                                              "10x", 1,
                                                              "5x", 2,
                                                              "+1000", 3,
                                                              "+500", 4,
                                                              "MISS", 5
                                                      ))
                                                      .build())
                                .build())
                .build();
        GameBoardGenerator generator = new GameBoardGenerator(new RandomGeneratorMock());
        Cell[][] board = generator.generate(configuration);

        Assertions.assertThat(getBoardSymbols(board))
                .isEqualTo(Map.of(
                        "+1000", 1,
                        "A", 1,
                        "B", 2,
                        "C", 3,
                        "D", 2
                ));
    }

    private List<CellSymbolProbabilities> getSymbolProbabilities(int rowCount, int cellCount) {
        List<CellSymbolProbabilities> cellSymbolProbabilities = new ArrayList<>();

        for (int row = 0; row < rowCount; row++) {
            for (int col = 0; col < cellCount; col++) {
                cellSymbolProbabilities.add(
                        CellSymbolProbabilities.builder()
                                .row(row)
                                .column(col)
                                .symbols(Map.of(
                                        "A", 1,
                                        "B", 2,
                                        "C", 3,
                                        "D", 4,
                                        "E", 5,
                                        "F", 6
                                ))
                                .build());
            }
        }

        return cellSymbolProbabilities;
    }

    private Map<String, Integer> getBoardSymbols(Cell[][] gameBoard) {
        Map<String, Integer> result = new HashMap<>();
        for (int row = 0; row < gameBoard.length; row++) {
            for (int col = 0; col < gameBoard[0].length; col++) {
                Cell boardCell = gameBoard[row][col];
                if (result.containsKey(boardCell.symbol())) {
                    result.computeIfPresent(boardCell.symbol(), (k, v) -> ++v);
                } else {
                    result.put(boardCell.symbol(), 1);
                }
            }
        }

        return result;
    }

    static class RandomGeneratorMock implements RandomGenerator {
        private int symbolCounter = 0;
        private int bonusCounter = 0;

        @Override
        public int nextInt(int max) {
            // symbol generation
            if (max == 21) {
                if (symbolCounter == max) {
                    symbolCounter = 0;
                }
                return symbolCounter++;
                // bonus generation
            } else if (max == 15) {
                return 3;
                // bonus position generation
            } else if (max == 3) {
                return 1;
            }

            throw new RuntimeException("Unexpected random request: " + max);
        }
    }
}