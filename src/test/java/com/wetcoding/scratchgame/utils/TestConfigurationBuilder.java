package com.wetcoding.scratchgame.utils;

import com.wetcoding.scratchgame.configuration.dto.Configuration;
import com.wetcoding.scratchgame.configuration.dto.ImpactType;
import com.wetcoding.scratchgame.configuration.dto.SymbolDefinition;
import com.wetcoding.scratchgame.configuration.dto.SymbolType;
import com.wetcoding.scratchgame.configuration.dto.combinations.CombinationGroupType;
import com.wetcoding.scratchgame.configuration.dto.combinations.CombinationParameters;
import com.wetcoding.scratchgame.configuration.dto.combinations.WhenConditionType;
import com.wetcoding.scratchgame.configuration.dto.probabilities.BonusProbabilities;
import com.wetcoding.scratchgame.configuration.dto.probabilities.CellSymbolProbabilities;
import com.wetcoding.scratchgame.configuration.dto.probabilities.Probabilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TestConfigurationBuilder {
    private static final Map<String, Integer> SYMBOL_PROBABILITIES = Map.of(
            "A", 1,
            "B", 2,
            "C", 3,
            "D", 4,
            "E", 5,
            "F", 6
    );

    private static final Map<String, Integer> BONUS_PROBABILITIES = Map.of(
            "10x", 1,
            "5x", 2,
            "+1000", 3,
            "+500", 4,
            "MISS", 5
    );

    private static final Configuration CONFIGURATION = Configuration.builder()
            .rows(3)
            .columns(3)
            .symbols(createSymbols())
            .probabilities(Probabilities.builder()
                                   .standardSymbols(createCellSymbolProbabilities())
                                   .bonusSymbols(BonusProbabilities.builder()
                                                         .symbols(BONUS_PROBABILITIES)
                                                         .build())
                                   .build())
            .winCombinations(createWithCombinations())
            .build();

    public static Configuration getExpectedConfiguration() {
        return CONFIGURATION;
    }

    private static Map<String, CombinationParameters> createWithCombinations() {
        Map<String, CombinationParameters> winCombinations = new HashMap<>();

        winCombinations.put("same_symbol_3_times",
                            CombinationParameters.builder()
                                    .rewardMultiplier(1f)
                                    .when(WhenConditionType.SAME_SYMBOLS)
                                    .count(3)
                                    .group(CombinationGroupType.SAME_SYMBOLS)
                                    .build());
        winCombinations.put("same_symbol_4_times",
                            CombinationParameters.builder()
                                    .rewardMultiplier(1.5f)
                                    .when(WhenConditionType.SAME_SYMBOLS)
                                    .count(4)
                                    .group(CombinationGroupType.SAME_SYMBOLS)
                                    .build());
        winCombinations.put("same_symbol_5_times",
                            CombinationParameters.builder()
                                    .rewardMultiplier(2f)
                                    .when(WhenConditionType.SAME_SYMBOLS)
                                    .count(5)
                                    .group(CombinationGroupType.SAME_SYMBOLS)
                                    .build());
        winCombinations.put("same_symbol_6_times",
                            CombinationParameters.builder()
                                    .rewardMultiplier(3f)
                                    .when(WhenConditionType.SAME_SYMBOLS)
                                    .count(6)
                                    .group(CombinationGroupType.SAME_SYMBOLS)
                                    .build());
        winCombinations.put("same_symbol_7_times",
                            CombinationParameters.builder()
                                    .rewardMultiplier(5f)
                                    .when(WhenConditionType.SAME_SYMBOLS)
                                    .count(7)
                                    .group(CombinationGroupType.SAME_SYMBOLS)
                                    .build());
        winCombinations.put("same_symbol_8_times",
                            CombinationParameters.builder()
                                    .rewardMultiplier(10f)
                                    .when(WhenConditionType.SAME_SYMBOLS)
                                    .count(8)
                                    .group(CombinationGroupType.SAME_SYMBOLS)
                                    .build());
        winCombinations.put("same_symbol_9_times",
                            CombinationParameters.builder()
                                    .rewardMultiplier(20f)
                                    .when(WhenConditionType.SAME_SYMBOLS)
                                    .count(9)
                                    .group(CombinationGroupType.SAME_SYMBOLS)
                                    .build());
        winCombinations.put("same_symbols_horizontally",
                            CombinationParameters.builder()
                                    .rewardMultiplier(2f)
                                    .when(WhenConditionType.LINEAR_SYMBOLS)
                                    .group(CombinationGroupType.HORIZONTALLY_LINEAR_SYMBOLS)
                                    .coveredAreas(Set.of(
                                            new CombinationParameters.CellPosition(0, 0), new CombinationParameters.CellPosition(0, 1), new CombinationParameters.CellPosition(0, 2),
                                            new CombinationParameters.CellPosition(1, 0), new CombinationParameters.CellPosition(1, 1), new CombinationParameters.CellPosition(1, 2),
                                            new CombinationParameters.CellPosition(2, 0), new CombinationParameters.CellPosition(2, 1), new CombinationParameters.CellPosition(2, 2)
                                    ))
                                    .build());
        winCombinations.put("same_symbols_vertically",
                            CombinationParameters.builder()
                                    .rewardMultiplier(2f)
                                    .when(WhenConditionType.LINEAR_SYMBOLS)
                                    .group(CombinationGroupType.VERTICALLY_LINEAR_SYMBOLS)
                                    .coveredAreas(Set.of(
                                            new CombinationParameters.CellPosition(0, 0), new CombinationParameters.CellPosition(0, 1), new CombinationParameters.CellPosition(0, 2),
                                            new CombinationParameters.CellPosition(1, 0), new CombinationParameters.CellPosition(1, 1), new CombinationParameters.CellPosition(1, 2),
                                            new CombinationParameters.CellPosition(2, 0), new CombinationParameters.CellPosition(2, 1), new CombinationParameters.CellPosition(2, 2)
                                    ))
                                    .build());
        winCombinations.put("same_symbols_diagonally_left_to_right",
                            CombinationParameters.builder()
                                    .rewardMultiplier(5f)
                                    .when(WhenConditionType.LINEAR_SYMBOLS)
                                    .group(CombinationGroupType.LTR_DIAGONALLY_LINEAR_SYMBOLS)
                                    .coveredAreas(Set.of(
                                            new CombinationParameters.CellPosition(0, 0), new CombinationParameters.CellPosition(1, 1), new CombinationParameters.CellPosition(2, 2)
                                    ))
                                    .build());
        winCombinations.put("same_symbols_diagonally_right_to_left",
                            CombinationParameters.builder()
                                    .rewardMultiplier(5f)
                                    .when(WhenConditionType.LINEAR_SYMBOLS)
                                    .group(CombinationGroupType.RTL_DIAGONALLY_LINEAR_SYMBOLS)
                                    .coveredAreas(Set.of(
                                            new CombinationParameters.CellPosition(0, 2), new CombinationParameters.CellPosition(1, 1), new CombinationParameters.CellPosition(2, 0)
                                    ))
                                    .build());

        return winCombinations;
    }

    private static Map<String, SymbolDefinition> createSymbols() {
        Map<String, SymbolDefinition> symbolsMap = new HashMap<>();
        symbolsMap.put("A", new SymbolDefinition(50f, null, SymbolType.STANDARD, null));
        symbolsMap.put("B", new SymbolDefinition(25f, null, SymbolType.STANDARD, null));
        symbolsMap.put("C", new SymbolDefinition(10f, null, SymbolType.STANDARD, null));
        symbolsMap.put("D", new SymbolDefinition(5f, null, SymbolType.STANDARD, null));
        symbolsMap.put("E", new SymbolDefinition(3f, null, SymbolType.STANDARD, null));
        symbolsMap.put("F", new SymbolDefinition(1.5f, null, SymbolType.STANDARD, null));
        symbolsMap.put("10x", new SymbolDefinition(10f, null, SymbolType.BONUS, ImpactType.MULTIPLY_REWARD));
        symbolsMap.put("5x", new SymbolDefinition(5f, null, SymbolType.BONUS, ImpactType.MULTIPLY_REWARD));
        symbolsMap.put("+1000", new SymbolDefinition(null, 1000f, SymbolType.BONUS, ImpactType.EXTRA_BONUS));
        symbolsMap.put("+500", new SymbolDefinition(null, 500f, SymbolType.BONUS, ImpactType.EXTRA_BONUS));
        symbolsMap.put("MISS", new SymbolDefinition(null, null, SymbolType.BONUS, ImpactType.MISS));

        return symbolsMap;
    }

    private static List<CellSymbolProbabilities> createCellSymbolProbabilities() {
        List<CellSymbolProbabilities> result = new ArrayList<>();
        for (int col = 0; col < 3; col++) {
            for (int row = 0; row < 3; row++) {

                result.add(CellSymbolProbabilities.builder()
                                   .row(row)
                                   .column(col)
                                   .symbols(SYMBOL_PROBABILITIES)
                                   .build());
            }
        }

        return result;
    }
}
