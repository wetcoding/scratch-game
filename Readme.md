# Home Assignment: Scratch Game #
### Environment:
- Java 17
- Maven

### Build:
```shell
mvn clean install
```

### Execute 
(from target directory)
```shell
cd target
java -jar scratch-game-1.0-SNAPSHOT-jar-with-dependencies.jar --config ../config.json --betting-amount 10
``` 